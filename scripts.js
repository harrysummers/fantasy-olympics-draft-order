$(document).ready(function () {
    var hasFirstWon = false;
    var hasSecondWon = false;
    var hasThirdWon = false;
    var hasFourthWon = false;

    function rank(place, team) {
        $('#result-list').append('<li class="list-group-item">' + place + '. ' + $(team).val() + '</li>');
    }

    function generateFirstPick() {
        var firstPickNumber = Math.floor((Math.random() * 100) + 1);
        if (firstPickNumber > 0 && firstPickNumber <= 40) {
            rank(1, '#firstTeamInput');
            hasFirstWon = true;
        } else if (firstPickNumber > 40 && firstPickNumber <= 70) {
            rank(1, '#secondTeamInput');
            hasSecondWon = true;
        } else if (firstPickNumber > 70 && firstPickNumber <= 90) {
            rank(1, '#thirdTeamInput');
            hasThirdWon = true;
        } else if (firstPickNumber > 90 && firstPickNumber <= 100) {
            rank(1, '#fourthTeamInput');
            hasFourthWon = true;
        }
    }

    function generateSecondPick() {
        var foundSecond = false;
        while (!foundSecond) {
            var secondPickNumber = Math.floor((Math.random() * 100) + 1);
            if (secondPickNumber > 0 && secondPickNumber <= 40 && !hasFirstWon) {
                rank(2, '#firstTeamInput');
                hasFirstWon = true;
                foundSecond = true;
            } else if (secondPickNumber > 40 && secondPickNumber <= 70 && !hasSecondWon) {
                rank(2, '#secondTeamInput');
                hasSecondWon = true;
                foundSecond = true;
            } else if (secondPickNumber > 70 && secondPickNumber <= 90 && !hasThirdWon) {
                rank(2, '#thirdTeamInput');
                hasThirdWon = true;
                foundSecond = true;
            } else if (secondPickNumber > 90 && secondPickNumber <= 100 && !hasFourthWon) {
                rank(2, '#fourthTeamInput');
                hasFourthWon = true;
                foundSecond = true;
            }
        }
    }

    function generateThirdPick() {
        var foundThird = false;
        while (!foundThird) {
            var thirdPickNumber = Math.floor((Math.random() * 100) + 1);
            if (thirdPickNumber > 0 && thirdPickNumber <= 40 && !hasFirstWon) {
                rank(3, '#firstTeamInput');
                hasFirstWon = true;
                foundThird = true;
            } else if (thirdPickNumber > 40 && thirdPickNumber <= 70 && !hasSecondWon) {
                rank(3, '#secondTeamInput');
                hasSecondWon = true;
                foundThird = true;
            } else if (thirdPickNumber > 70 && thirdPickNumber <= 90 && !hasThirdWon) {
                rank(3, '#thirdTeamInput');
                hasThirdWon = true;
                foundThird = true;
            } else if (thirdPickNumber > 90 && thirdPickNumber <= 100 && !hasFourthWon) {
                rank(3, '#fourthTeamInput');
                hasFourthWon = true;
                foundThird = true;
            }
        }
    }

    function generateFourthPick() {
        if (!hasFirstWon) {
            rank(4, '#firstTeamInput');
        } else if (!hasSecondWon) {
            rank(4, '#secondTeamInput');
        } else if (!hasThirdWon) {
            rank(4, '#thirdTeamInput');
        } else {
            rank(4, '#fourthTeamInput');
        }
    }

    function generateRest() {
        rank(5, '#fifthTeamInput');
        rank(6, '#sixthTeamInput');
        rank(7, '#seventhTeamInput');
        rank(8, '#eighthTeamInput');
    }

    function reset() {
        hasFirstWon = false;
        hasSecondWon = false;
        hasThirdWon = false;
        hasFourthWon = false;
        $('#result-list').html('');
    }

    $('#start').click(function () {
        reset();
        generateFirstPick();
        generateSecondPick();
        generateThirdPick();
        generateFourthPick();
        generateRest();
    })
});